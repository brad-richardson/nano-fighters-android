package com.bakercrew.nanofighter;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Brad on 4/21/2015.
 */
public class RealmMatch extends RealmObject{

    private String parseId;
    private String results;

    public String getParseId() {
        return parseId;
    }

    public void setParseId(String parseId) {
        this.parseId = parseId;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }
}
