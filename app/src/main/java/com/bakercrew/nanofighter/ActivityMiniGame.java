package com.bakercrew.nanofighter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;


public class ActivityMiniGame extends Activity implements View.OnTouchListener {

    Context mContext;
    MyApplication myApp;

    CountDownTimer mCountDownTimer;

    ImageView mFighterImageView;
    TextView mStateTextView;
    Button mButton1, mButton2;
    Drawable mButtonBackground;

    Handler mHandler;
    Runnable mLiftRunnable;

    long mButton1Pressed, mButton2Pressed;
    long mLiftDelayNs;
    int lifts = 0;

    boolean mIsLifted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mini_game);

        mFighterImageView = (ImageView) findViewById(R.id.fighterView);
        mStateTextView = (TextView) findViewById(R.id.textView);
        mStateTextView.setText("Click the button to start!");
        mButton1 = (Button) findViewById(R.id.mashButton1);
        mButton2 = (Button) findViewById(R.id.mashButton2);
        mButton1.setOnTouchListener(this);
        mButton2.setOnTouchListener(this);

        myApp = (MyApplication) getApplication();
        mContext = this;

        mButtonBackground = mButton1.getBackground();

        try {
            mLiftDelayNs =
                    (long) (1000000000 * myApp.configJson
                            .getJSONObject("training")
                            .getJSONObject("strength")
                            .getDouble("buttonDelayTime"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setup();
    }

    protected void onResume() {
        super.onResume();
        mCountDownTimer.start();
    }

    protected void onPause() {
        super.onPause();
        mCountDownTimer.cancel();
    }

    private void setup() {
        long trainTimeMs = 0;
        try {
            trainTimeMs = (long) (1000 * myApp.configJson
                    .getJSONObject("training")
                    .getJSONObject("strength")
                    .getDouble("trainTime"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mCountDownTimer = new CountDownTimer(trainTimeMs, 1000) {
            @Override
            public void onTick(long millisUntilFinished) { }

            @Override
            public void onFinish() {
                // do calc of xp gained
                double strGained = lifts * 100;
                String toShow = "You gained: " + strGained + " strength! - FIX THIS";
                Toast.makeText(mContext, toShow, Toast.LENGTH_SHORT).show();

                RealmFighterUtils.addStrength(mContext, myApp.currFighter, strGained);

                finish();
            }
        };

        mHandler = new Handler();
        mLiftRunnable = new Runnable() {

            @Override
            public void run() {
                endLift();
            }
        };
    }

    private void doLift() {
        mFighterImageView.setImageResource(R.drawable.sumoweight2);
        mIsLifted = true;
    }

    private void endLift() {
        mFighterImageView.setImageResource(R.drawable.sumoweight1);
        mIsLifted = false;
    }

    private void checkForSuccessfulLift() {
        // Pressed the buttons within time - THIS IS IN NANOSECONDS!!
        if(Math.abs(mButton1Pressed - mButton2Pressed) < mLiftDelayNs) {
            lifts++;
            mButton1Pressed = 0;
            mButton2Pressed = 0;
            mStateTextView.setText("Lifts: " + lifts);

            mButton1.setBackgroundColor(getResources().getColor(R.color.green));
            mButton2.setBackgroundColor(getResources().getColor(R.color.green));

            // Cancel any previous callbacks and post new delayed update of image
            mHandler.removeCallbacks(mLiftRunnable);
            mHandler.postDelayed(mLiftRunnable, 150);

            if(!mIsLifted) {
                doLift();
            }
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP) {
            mButton1.setBackground(mButtonBackground);
            mButton2.setBackground(mButtonBackground);
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (v.equals(mButton1)) {
                mButton1Pressed = System.nanoTime();
                //v.setBackgroundColor(getResources().getColor(R.color.yellow));
                checkForSuccessfulLift();
            } else {
                mButton2Pressed = System.nanoTime();
                //v.setBackgroundColor(getResources().getColor(R.color.yellow));
                checkForSuccessfulLift();
            }
        }
        return true;
    }

    public void doNothing(View view) {
        // Only so the buttons will animate
    }
}
