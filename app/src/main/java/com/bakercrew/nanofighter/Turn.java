package com.bakercrew.nanofighter;

import io.realm.RealmObject;

/**
 * Created by Brad on 4/23/2015.
 */
public class Turn extends RealmObject {

    private String fighter1, fighter2;
    private double damage1, damage2;

    public String getFighter1() {
        return fighter1;
    }

    public void setFighter1(String fighter1) {
        this.fighter1 = fighter1;
    }

    public String getFighter2() {
        return fighter2;
    }

    public void setFighter2(String fighter2) {
        this.fighter2 = fighter2;
    }

    public double getDamage1() {
        return damage1;
    }

    public void setDamage1(double damage1) {
        this.damage1 = damage1;
    }

    public double getDamage2() {
        return damage2;
    }

    public void setDamage2(double damage2) {
        this.damage2 = damage2;
    }
}
