package com.bakercrew.nanofighter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;


public class ActivityMatchResults extends Activity {

    TextView mTextView;

    MyApplication myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_match_results);

        mTextView = (TextView) findViewById(R.id.textViewMatchResults);

        myApp = (MyApplication) getApplication();

        mTextView.setText(RealmFighterUtils.listMatches(myApp.currFighter));
    }

}
