package com.bakercrew.nanofighter;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Brad on 4/18/2015.
 */
public class MyApplication extends Application {

    RealmFighter currFighter;
    String parseUserId;

    JSONObject configJson;

    @Override
    public void onCreate() {
        Parse.initialize(this, "PpOZNgeTiEl5TrNHjKHcpX9hyNDgQkEWLrgFvk8n", "rVRfZPXdIuoKFjClB9NeDgJnJKhVZvzGyn6rBkzW");

        //Read in JSON config file
        //config = new Config();
        readConfigFile();

        getUserId();
    }

    public void readConfigFile() {
        String file = null;
        try {
            InputStream inputStream = getAssets().open("shared/Configuration.JSON");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();

            file = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        try {
            //JSONObject json = new JSONObject(file);

            configJson = new JSONObject(file);

//            // Training
//            JSONObject training = json.getJSONObject("training");
//
//            // Strength
//            JSONObject strength = training.getJSONObject("strength");
//            config.STRENGTH_TRAINTIME = strength.getDouble("trainTime");
//            config.STRENGTH_BUTTONDELAYTIME = strength.getDouble("buttonDelayTime");
//            config.STRENGTH_INCREMENTAMOUNT = strength.getDouble("incrementAmount");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getUserId() {
        // check shared preferences, if doesn't exist, get new parse id
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        parseUserId = preferences.getString(Util.PREFS_USERIDKEY, "");

        if(parseUserId == "") {
            final ParseObject user = new ParseObject(Util.PARSE_USER_OBJECT);
            user.put(Util.PARSE_USER_DEVICE, Build.MODEL);
            user.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    parseUserId = user.getObjectId();
                    saveUserId();
                    Log.d(Util.LOGTAG, "User created: " + parseUserId);
                }
            });
        }
    }

    private void saveUserId() {
        //store to shared preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Util.PREFS_USERIDKEY, parseUserId);
        editor.apply();
    }

}
