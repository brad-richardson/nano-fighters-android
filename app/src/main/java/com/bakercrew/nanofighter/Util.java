package com.bakercrew.nanofighter;

import android.content.Context;
import android.util.Log;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Brad on 4/18/2015.
 */
public class Util {
    static final String PARSE_USER_OBJECT = "testUser";
    static final String PARSE_USER_DEVICE = "device";

    static final String PARSE_FIGHTER_OBJECT = "testFighter";
    static final String PARSE_FIGHTER_ID = "objectId";
    static final String PARSE_FIGHTER_IDUSER = "idUser";
    static final String PARSE_FIGHTER_NAME = "name";
    static final String PARSE_FIGHTER_CHECK1 = "check1";
    static final String PARSE_FIGHTER_READY = "readyToFight";
    static final String PARSE_FIGHTER_JSON = "json";

    static final String PARSE_MATCH_OBJECT = "testMatch";
    static final String PARSE_MATCH_FIGHTER1 = "idFighter1";
    static final String PARSE_MATCH_FIGHTER2 = "idFighter2";
    static final String PARSE_MATCH_RESULTS = "results";

    static final String PREFS_USERIDKEY = "userId";

    static final String LOGTAG = "NanoFighters";


}
