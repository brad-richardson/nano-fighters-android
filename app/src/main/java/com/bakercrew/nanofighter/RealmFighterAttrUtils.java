package com.bakercrew.nanofighter;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by Brad on 4/28/2015.
 */
public class RealmFighterAttrUtils {

    //Add to current max values for level up
    public static void levelUp() {

    }

    //Needs # of lifts
    public static void addStrengthTraining(int lifts) {

    }

    //Needs quantity of things done
    public static void addSpeedTraining(int amt) {

    }

    public static void addFeeding() {

    }

    public static void addSleeping() {

    }

    //Pass in number of minutes that have elapsed
    public static void doTimeDrain(double elapsedMinutes) {

    }

    public static void doPostMatchChanges(boolean didWin) {

    }

    //Local methods that change values in fighter
    private static void addStrength(RealmFighter rf, double str) {
        if(rf.getCurrentStr() + str >= rf.getMaxStr()) {
            rf.setCurrentStr(rf.getMaxStr());
        } else {
            rf.setCurrentStr(rf.getCurrentStr() + str);
        }
    }

    private static void addSpeed(RealmFighter rf, double speed) {
        if(rf.getCurrentSpeed() + speed >= rf.getMaxSpeed()) {
            rf.setCurrentSpeed(rf.getMaxSpeed());
        } else {
            rf.setCurrentSpeed(rf.getCurrentSpeed() + speed);
        }
    }

    private static void addFeed(RealmFighter rf, double feed) {
        if(rf.getCurrentFed() + feed >= rf.getMaxFed()) {
            rf.setCurrentFed(rf.getMaxFed());
        } else {
            rf.setCurrentFed(rf.getCurrentFed() + feed);
        }
    }

    private static void addRest(RealmFighter rf, double rest) {
        if(rf.getCurrentRest() + rest >= rf.getMaxRest()) {
            rf.setCurrentRest(rf.getMaxRest());
        } else {
            rf.setCurrentRest(rf.getCurrentRest() + rest);
        }
    }
}
