package com.bakercrew.nanofighter;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;


public class ActivityHome extends Activity {

    ViewSwitcher mViewSwitcher;
    ImageView mFighterView, mFoodView, mEatingView;
    TextView mLevelTextView, mHpTextView;
    ProgressBar mStrProgress, mSpeedProgress, mFedProgress, mRestProgress, mXpProgress;

    MyApplication myApp;
    Realm mRealm;
    Context mContext;

    CountDownTimer mFeedTimer;
    boolean mIsFeeding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mViewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        mFighterView = (ImageView) findViewById(R.id.fighterView);
        mFoodView = (ImageView) findViewById(R.id.foodView);
        mEatingView = (ImageView) findViewById(R.id.eatingView);

        mLevelTextView = (TextView) findViewById(R.id.lvlTextView);
        mHpTextView = (TextView) findViewById(R.id.hpTextView);

        mStrProgress = (ProgressBar) findViewById(R.id.strProgress);
        mSpeedProgress = (ProgressBar) findViewById(R.id.speedProgress);
        mFedProgress = (ProgressBar) findViewById(R.id.fedProgress);
        mRestProgress = (ProgressBar) findViewById(R.id.restProgress);
        mXpProgress = (ProgressBar) findViewById(R.id.xpProgress);

        myApp = (MyApplication)this.getApplication();
        mRealm = Realm.getInstance(this);
        mContext = this;

        setTitle(myApp.currFighter.getName() + " (" + myApp.currFighter.getParseId() + ")");

        try {
            setup();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void onResume() {
        super.onResume();

        updateStats();
        RealmFighterUtils.checkForMatch(this, myApp.currFighter);

        // Start fighter moving
        ((AnimationDrawable) mFighterView.getBackground()).start();
    }

    private void setup() throws JSONException {
        JSONObject json = myApp.configJson;
        int maxLevel = json.getInt("maxLevel")-1;

        JSONObject attr = json.getJSONObject("attributes");

        double initStr = attr.getJSONObject("strength").getDouble("initial");
        double stepStr = attr.getJSONObject("strength").getDouble("change");
        double initSpeed = attr.getJSONObject("speed").getDouble("initial");
        double stepSpeed = attr.getJSONObject("speed").getDouble("change");
        double initFeed = attr.getJSONObject("feed").getDouble("initial");
        double stepFeed = attr.getJSONObject("feed").getDouble("change");
        double initRest = attr.getJSONObject("rest").getDouble("initial");
        double stepRest = attr.getJSONObject("rest").getDouble("change");

        int maxStr = (int) (initStr + stepStr*maxLevel);
        mStrProgress.setMax(maxStr);
        int maxSpeed = (int) (initSpeed + stepSpeed*maxLevel);
        mSpeedProgress.setMax(maxSpeed);
        int maxFeed = (int) (initFeed + stepFeed*maxLevel);
        mFedProgress.setMax(maxFeed);
        int maxRest = (int) (initRest + stepRest*maxLevel);
        mRestProgress.setMax(maxRest);

        mXpProgress.setSecondaryProgress(100);

    }

    private void updateStats() {
        RealmFighter rf = myApp.currFighter;

        mStrProgress.setProgress((int) rf.getCurrentStr());
        mStrProgress.setSecondaryProgress((int) rf.getMaxStr());
        mSpeedProgress.setProgress((int) rf.getCurrentSpeed());
        mSpeedProgress.setSecondaryProgress((int) rf.getMaxSpeed());
        mFedProgress.setProgress((int) rf.getCurrentFed());
        mFedProgress.setSecondaryProgress((int) rf.getMaxFed());
        mRestProgress.setProgress((int) rf.getCurrentRest());
        mRestProgress.setProgress((int) rf.getCurrentRest());

        mXpProgress.setProgress(RealmFighterUtils.xpProgress(myApp.currFighter));

        mLevelTextView.setText("Lvl: " + myApp.currFighter.getCurrentLevel());
        mHpTextView.setText("HP: " + myApp.currFighter.getCurrentHP());
    }

    public void sendFighter(View view) {
        RealmFighterUtils.updateParseFighterObject(this, myApp.currFighter, true);

    }

    public void feedFighter(View view) {
        if(!mIsFeeding) {
            mIsFeeding = true;
            // Switch views and start animation
            mViewSwitcher.showNext();
            ((AnimationDrawable) mFoodView.getBackground()).start();
            ((AnimationDrawable) mEatingView.getBackground()).start();

            mFeedTimer = new CountDownTimer(5000,1000) {
               @Override
               public void onTick(long millisUntilFinished) {

               }

               @Override
               public void onFinish() {
                    // Stop animations, switch views and apply eating
                    ((AnimationDrawable) mFoodView.getBackground()).stop();
                    ((AnimationDrawable) mEatingView.getBackground()).stop();
                    mViewSwitcher.showNext();

                    mIsFeeding = false;
               }
            }.start();
        }
    }

    public void launchMinigame(View view) {
        Intent intent = new Intent(mContext, ActivityMiniGame.class);
        startActivity(intent);
    }

    public void showResults() {
        Intent intent = new Intent(this, ActivityMatchResults.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_matches:
                showResults();
                break;
            default:
                break;
        }

        return true;
    }
}
