package com.bakercrew.nanofighter;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Brad on 4/18/2015.
 */
public class RealmFighter extends RealmObject {

    // Identification
    private String parseId;
    private String name;

    // Current values
    private double currentHP;
    private int currentLevel;
    private double currentXP;
    private double currentStr;
    private double currentSpeed;
    private double currentFed;
    private double currentRest;

    // Max values
    private double maxHP;
    private double maxStr;
    private double maxSpeed;
    private double maxFed;
    private double maxRest;

    // Data
    private boolean inMatch;
    private RealmList<RealmMatch> matches;

    // Getters and Setters

    public void setParseId(String parseId) {
        this.parseId = parseId;
    }

    public String getParseId() {
        return parseId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(double currentHP) {
        this.currentHP = currentHP;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public double getCurrentXP() {
        return currentXP;
    }

    public void setCurrentXP(double currentXP) {
        this.currentXP = currentXP;
    }

    public double getMaxRest() {
        return maxRest;
    }

    public void setMaxRest(double maxRest) {
        this.maxRest = maxRest;
    }

    public double getCurrentStr() {
        return currentStr;
    }

    public void setCurrentStr(double currentStr) {
        this.currentStr = currentStr;
    }

    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(double currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public double getCurrentFed() {
        return currentFed;
    }

    public void setCurrentFed(double currentFed) {
        this.currentFed = currentFed;
    }

    public double getCurrentRest() {
        return currentRest;
    }

    public void setCurrentRest(double currentRest) {
        this.currentRest = currentRest;
    }

    public double getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(double maxHP) {
        this.maxHP = maxHP;
    }

    public double getMaxStr() {
        return maxStr;
    }

    public void setMaxStr(double maxStr) {
        this.maxStr = maxStr;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getMaxFed() {
        return maxFed;
    }

    public void setMaxFed(double maxFed) {
        this.maxFed = maxFed;
    }

    public boolean getInMatch() {
        return inMatch;
    }

    public void setInMatch(boolean inMatch) {
        this.inMatch = inMatch;
    }

    public RealmList<RealmMatch> getMatches() {
        return matches;
    }

    public void setMatches(RealmList<RealmMatch> matches) {
        this.matches = matches;
    }
}
