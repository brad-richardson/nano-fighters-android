package com.bakercrew.nanofighter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Brad on 4/18/2015.
 */
public class RecyclerViewAdapterFighter extends RecyclerView.Adapter<RecyclerViewAdapterFighter.ViewHolder>{

    Context mContext;

    Realm mRealm;

    public RecyclerViewAdapterFighter (Context context) {
        mContext = context;
        mRealm = Realm.getInstance(mContext);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView mNameTextView;
        protected TextView mStatusTextView;
        protected RealmFighter realmFighter;

        public ViewHolder(CardView v) {
            super(v);
            mNameTextView = (TextView) v.findViewById(R.id.textViewName);
            mStatusTextView = (TextView) v.findViewById(R.id.textViewStatus);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Get object, store to application
                    //RealmFighter realmFighter = mRealm.where(RealmFighter.class).equalTo("parseId", parseId).findFirst();
                    MyApplication myApp = (MyApplication) mContext.getApplicationContext();
                    myApp.currFighter = realmFighter;

                    Intent intent = new Intent(mContext, ActivityHome.class);
                    mContext.startActivity(intent);
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                    alert.setMessage("Delete user?");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Unsubscribe from push notifications for fighter
                            ParsePush.unsubscribeInBackground("F" + realmFighter.getParseId());

                            // Delete fighter from fighter table
                            ParseQuery<ParseObject> query = ParseQuery.getQuery(Util.PARSE_FIGHTER_OBJECT);
                            query.whereEqualTo(Util.PARSE_FIGHTER_ID, realmFighter.getParseId());
                            query.getFirstInBackground(new GetCallback<ParseObject>() {
                                @Override
                                public void done(ParseObject parseObject, ParseException e) {
                                    if(e != null) {
                                        Log.d(Util.LOGTAG, "Parse error (delete fighter): " + e.getMessage());
                                    }
                                    if(parseObject != null) {
                                        parseObject.deleteInBackground();
                                    }
                                }
                            });

                            // Remove fighter from realm
                            mRealm.beginTransaction();
                            realmFighter.removeFromRealm();
                            mRealm.commitTransaction();

                            // Reset list view
                            ((ActivityFighterList)mContext).setAdapter();
                        }
                    });
                    alert.setNegativeButton("Cancel", null);
                    alert.show();

                    return true;
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_fighter, viewGroup, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder((CardView)v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        RealmResults<RealmFighter> results = mRealm.where(RealmFighter.class).findAll();
        RealmFighter rf = results.get(i);
        viewHolder.mNameTextView.setText(rf.getName());
        viewHolder.mStatusTextView.setText("Status: " + RealmFighterUtils.getStatus(rf));
        viewHolder.realmFighter = rf;
    }

    @Override
    public int getItemCount() {
        return mRealm.where(RealmFighter.class).findAll().size();
    }
}
