package com.bakercrew.nanofighter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;


public class ActivityFighterList extends Activity {

    RecyclerViewAdapterFighter mRecyclerViewAdapter;
    private RecyclerView mRecyclerView;

    Realm mRealm;

    MyApplication myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fighter_list);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        myApp = (MyApplication) this.getApplication();
        mRealm = Realm.getInstance(this);

        setTitle("Fighter List");

        RealmFighter fighter = mRealm.where(RealmFighter.class).findFirst();
        if(fighter == null) {
            promptForNewFighter();
        }

    }

    protected void onResume() {
        super.onResume();
        setAdapter();
    }

    public void setAdapter() {
        mRecyclerViewAdapter = new RecyclerViewAdapterFighter(this);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_fighter_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                promptForNewFighter();
                break;
            default:
                break;
        }

        return true;
    }

    public void promptForNewFighter() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Enter name for your new fighter:");

        final EditText input = new EditText(this);
        // Layout for margins
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 0, 10, 0);
        input.setLayoutParams(layoutParams);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        input.setHint("Name");

        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createNewFighter(input.getText().toString());
            }
        });
        alert.setNegativeButton("Cancel", null);
        alert.show();
    }

    public void createNewFighter(String fighterName) {
        // Create fighter
        RealmFighter newFighter = null;
        try {
            newFighter = RealmFighterUtils.createFighter(this, mRealm);
        } catch (JSONException e) {
            e.printStackTrace();
            showFighterCreationError();
            return;
        }
        mRealm.beginTransaction();
        newFighter.setName(fighterName);
        mRealm.commitTransaction();

        myApp.currFighter = newFighter;

        //Prep parse object
        final ParseObject parseFighter = new ParseObject(Util.PARSE_FIGHTER_OBJECT);
        parseFighter.put(Util.PARSE_FIGHTER_IDUSER, myApp.parseUserId);
        parseFighter.put(Util.PARSE_FIGHTER_NAME, myApp.currFighter.getName());
        parseFighter.put(Util.PARSE_FIGHTER_JSON, RealmFighterUtils.getParseJsonForFighter(myApp.currFighter));
        parseFighter.put(Util.PARSE_FIGHTER_CHECK1, 1);
        parseFighter.put(Util.PARSE_FIGHTER_READY, 0);

        // Sends fighter to realm synchronously
        showFighterLoadDialog(parseFighter);
    }

    public void showFighterLoadDialog(final ParseObject parseFighter) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "Please wait...",	"Preparing fighter", true);
        ringProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    parseFighter.save();
                } catch (ParseException e) {
                    // Failed to save, remove fighter and show failure
                    deleteFighterAndUpdate();

                    ringProgressDialog.dismiss();
                    e.printStackTrace();
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Update fighter ID
                        mRealm.beginTransaction();
                        myApp.currFighter.setParseId(parseFighter.getObjectId());
                        mRealm.commitTransaction();
                    }
                });

                // Subscribe for push notifications of fighter
                ParsePush.subscribeInBackground("F" + parseFighter.getObjectId());

                Log.d(Util.LOGTAG, "Fighter added: " + parseFighter.getObjectId());

                ringProgressDialog.dismiss();
            }
        }).start();
    }

    public void showFighterCreationError() {
        Toast.makeText(this, "Unable to create fighter! Try again.", Toast.LENGTH_LONG).show();
    }

    public void deleteFighterAndUpdate() {
        // Must be run on main thread for realm

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                mRealm.beginTransaction();
                myApp.currFighter.removeFromRealm();
                mRealm.commitTransaction();

                //Update view
                //setAdapter();

                showFighterCreationError();
            }
        };
        runOnUiThread(runnable);
    }

}
