package com.bakercrew.nanofighter;

import android.content.Context;
import android.util.Log;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Brad on 4/18/2015.
 */
public class RealmFighterUtils {

    public static RealmFighter createFighter(Context context, Realm realm) throws JSONException {
        MyApplication myApp = (MyApplication) context.getApplicationContext();
        JSONObject json = myApp.configJson;
        JSONObject attr = json.getJSONObject("attributes");

        realm.beginTransaction();
        RealmFighter rf = realm.createObject(RealmFighter.class);

        rf.setCurrentLevel(1);

        rf.setMaxHP(attr.getJSONObject("hitpoints").getDouble("initial"));
        rf.setMaxStr(attr.getJSONObject("strength").getDouble("initial"));
        rf.setMaxSpeed(attr.getJSONObject("speed").getDouble("initial"));
        rf.setMaxFed(attr.getJSONObject("feed").getDouble("initial"));
        rf.setMaxRest(attr.getJSONObject("rest").getDouble("initial"));

        rf.setCurrentHP(rf.getMaxHP());
        rf.setCurrentStr(rf.getMaxStr());
        rf.setCurrentSpeed(rf.getMaxSpeed());
        rf.setCurrentFed(rf.getMaxFed());
        rf.setCurrentRest(rf.getMaxRest());

        realm.commitTransaction();

        return rf;
    }

    public static double getLevelXp(int currLevel) {
        return Math.pow(10*(currLevel+1), 1.5);
    }

    //returns true if it leveled up
    public static boolean addXp(Context context, RealmFighter rf, double xpToAdd) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();

        boolean didLevelUp = false;
        while(xpToAdd > 0) {
            // Calculate amount to get to next level
            double nextLevelXp = getLevelXp(rf.getCurrentLevel());

            if(rf.getCurrentXP() + xpToAdd >= nextLevelXp) {
                didLevelUp = true;
                xpToAdd -= nextLevelXp - rf.getCurrentXP();

                //Set new fighter values
                rf.setCurrentXP(nextLevelXp);
                rf.setCurrentLevel(rf.getCurrentLevel() + 1);
            } else {
                rf.setCurrentXP(rf.getCurrentXP()+ xpToAdd);
                xpToAdd = 0;
            }
        }

        realm.commitTransaction();
        return didLevelUp;
    }

    public static void addStrength(Context context, RealmFighter rf, double strToAdd) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        rf.setCurrentStr(rf.getCurrentStr() + strToAdd);
        realm.commitTransaction();
    }

    public static int xpProgress(RealmFighter rf) {
        return (int) (rf.getCurrentXP()/getLevelXp(rf.getCurrentLevel()) * 100);
    }

    public static void updateParseFighterObject(final Context context, final RealmFighter realmFighter, final boolean readyToFight) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Util.PARSE_FIGHTER_OBJECT);
        query.whereEqualTo(Util.PARSE_FIGHTER_ID, realmFighter.getParseId());
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e != null) {
                    Log.d(Util.LOGTAG, "Update parse fighter (error): " + e.getMessage());
                }
                if(parseObject != null) {
                    // Update parse values and push back up
                    parseObject.put(Util.PARSE_FIGHTER_READY, (readyToFight? 1 : 0));
                    parseObject.put(Util.PARSE_FIGHTER_JSON, getParseJsonForFighter(realmFighter));
                    parseObject.saveInBackground();

                    //Store that fighter is in match
                    Realm realm = Realm.getInstance(context);
                    realm.beginTransaction();
                    realmFighter.setInMatch(true);
                    realm.commitTransaction();
                }
            }
        });
    }

    // Checks parse to see if a match has been finished
    public static void checkForMatch(final Context context, final RealmFighter realmFighter) {
        ParseQuery<ParseObject> idCol1 = ParseQuery.getQuery(Util.PARSE_MATCH_OBJECT);
        idCol1.whereEqualTo(Util.PARSE_MATCH_FIGHTER1, realmFighter.getParseId());

        ParseQuery<ParseObject> idCol2 = ParseQuery.getQuery(Util.PARSE_MATCH_OBJECT);
        idCol2.whereEqualTo(Util.PARSE_MATCH_FIGHTER2, realmFighter.getParseId());

        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
        queries.add(idCol1);
        queries.add(idCol2);

        ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
        mainQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e != null) {
                    //Log.d(Util.LOGTAG, "Parse error (match): " + e.getMessage());

                    // No match found
                    return;
                }

                Log.d(Util.LOGTAG, "Match found: " + parseObject.getObjectId());

                Realm realm = Realm.getInstance(context);
                realm.beginTransaction();

                //Create new match object and store onto fighter for later
                RealmMatch match = realm.createObject(RealmMatch.class);
                match.setParseId(parseObject.getObjectId());
                match.setResults(parseObject.getString(Util.PARSE_MATCH_RESULTS));

                //Fighter is no longer in a match, save it for later
                realmFighter.setInMatch(false);
                realmFighter.getMatches().add(match);

                realm.commitTransaction();

                // Clear id from parseDB match table based on which col it is
                boolean isFirstCol = parseObject.get(Util.PARSE_MATCH_FIGHTER1).equals(realmFighter.getParseId());
                parseObject.put((isFirstCol ? Util.PARSE_MATCH_FIGHTER1 : Util.PARSE_MATCH_FIGHTER2), "");
                parseObject.saveInBackground();
            }
        });
    }

    static String getStatus(RealmFighter realmFighter) {
        if(realmFighter.getInMatch()) {
            return "Off to war!";
        } else {
            return "Happy!";
        }
    }

    static String listMatches(RealmFighter rf) {
        RealmList<RealmMatch> matches = rf.getMatches();
        StringBuilder stringBuilder = new StringBuilder();
        for(RealmMatch match : matches) {
            stringBuilder.append("Match:\n" + match.getResults() + "\n");
        }
        return stringBuilder.toString();
    }

    static JSONObject getParseJsonForFighter(RealmFighter rf) {
        JSONObject json = new JSONObject();
        try {
            json.put("fed", rf.getCurrentFed());
            json.put("hp", rf.getCurrentHP());
            json.put("level", rf.getCurrentLevel());
            json.put("rest", rf.getCurrentRest());
            json.put("speed", rf.getCurrentSpeed());
            json.put("strength", rf.getCurrentStr());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String toString(RealmFighter rf) {
        //double hpPercent = rf.getCurrentHP() / MAX_HP;

        //Calculate current level progress
        double prevLvlXp = getLevelXp(rf.getCurrentLevel());
        double currLvlRange = getLevelXp(rf.getCurrentLevel()+1) - prevLvlXp;
        double currLvlProgress = rf.getCurrentXP() - prevLvlXp;
        double xpPercent = currLvlProgress / currLvlRange;
//        Log.d(Util.LOGTAG, "Prev lvl xp: " +prevLvlXp);
//        Log.d(Util.LOGTAG, "Next lvl xp: " + rf.getNextLevelXP());
//        Log.d(Util.LOGTAG, "Curr XP: " + rf.getCurrentXP());
//        Log.d(Util.LOGTAG, "Curr lvl progress: " + currLvlProgress);
//
//        Log.d(Util.LOGTAG, "Ratios: hp/xp: " + hpPercent + "/" + xpPercent);

        StringBuilder stringBuilder = new StringBuilder();
        //stringBuilder.append("Status:\n" + getStatus(rf));
//        stringBuilder.append("\nHP:\n");
//        for(int i = 0; i < 10; i++) {
//            if(hpPercent >= i/10.0) {
//                stringBuilder.append("X");
//            } else {
//                stringBuilder.append("--");
//            }
//        }
        stringBuilder.append("\nXP:\n");
        for(int i = 0; i < 10; i++) {
            if(xpPercent >= i/10.0) {
                stringBuilder.append("X");
            } else {
                stringBuilder.append("--");
            }
        }
        stringBuilder.append("\nId:\n" + rf.getParseId());

        return stringBuilder.toString();
    }

}
